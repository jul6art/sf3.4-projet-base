﻿http://akivaron.github.io/miminium/

# theme
    - route pour supprimer images

    - flex masonry lists

    - design
        + roulette (attention taille de l'image doit bien prendre la taille du conteneur)
        
    ## gestion users profile
    - messages, favoris et fans dans espace notifs (ATTENTION MOBILE MENU)header de page bikes (garage + login div)
    - page profile 
        + card profile user pour tab user dans recherche (résultats recherche users)
        + panels pour messages favoris et fans (et scroll overflow-y scroll)
        + revoir icone avatar, intégrer une tooltip plus grosse style card
    - garage copie html de page mes motos 
        + dans menu si app.user
        + rappel de la porte de garage?
        + box user possesseur du garage
        + box social pour interactions avec le possesseur (sauf entre lui et lui ..)
        + ATTENTION REAL TIME UPDATE COTE sur motos
    - vue moto + images fancybox (voir page photos)
        + cote(étoiles en grand)
        + box user possesseur du garage
        + box social pour interactions avec le possesseur (sauf entre lui et lui ..)
        + ATTENTION REAL TIME UPDATE COTE sur motos, photos, ...
    - page about + box user à mon effigie
    - tester workflow en non app_dev après assetic:dump --env=prod

# pre hosting
    - reprendre database en l'état 
            + éventuels new users de prod 
            + image moto Jona de prod
            + images dans /data/uploads

# post hosting
    - creer virtual server pour v1.cmybike.com qui pointe vers la meme db que cmybike nouvelle version
    - sur serveur, passer un doctrine:schema:update --force
    - tester sur serveur bug HTTPS SSL form post (search, photo, video, ...)
    - tester workflow en prod après assetic:dump --env=prod
    - activer email verification register et template email light (POUR TOUTES NOTIFICATIONS PAR MAIL (FOS ...)

# publicité
    - article et projet vsweb et modifier ancien projet pour signaler old version + url
    - envoyer message greed et autres pour leurs motos ou pub ou leurs vidéos (amour de la moto, musée, ..., existe depuis 4ans, sera toujours là, vous pourrez tjrs voir vos motos)
    - 2ememain
    
## Version 3
    - notifications push: https://www.blogdumoderateur.com/notifications-push-chrome-firefox/