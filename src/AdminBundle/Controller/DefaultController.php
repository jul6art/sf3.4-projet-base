<?php

namespace AdminBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package AdminBundle\Controller
 */
class DefaultController extends FOSRestController
{
	/**
	 * @Route("/", name="admin_home")
	 */
	public function indexAction(Request $request)
	{
		$view = $this->view()
		             ->setTemplate('@Admin/Default/index.html.twig');

		return $this->handleView($view);
	}
}
