<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

/**
 * Class DefaultController
 * @package ApiBundle\Controller
 */
class DefaultController extends FOSRestController
{

}
