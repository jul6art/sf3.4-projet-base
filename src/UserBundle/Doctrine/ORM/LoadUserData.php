<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 20/08/2018
 * Time: 21:27
 */

namespace UserBundle\Doctrine\ORM;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Entity\User;

/**
 * Class LoadUserData
 * @package UserBundle\Doctrine\ORM
 */
class LoadUserData extends Fixture
{
	/**
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager)
	{
		// User
		$user = $this->createUser('user', 'user', 'user@test.com');
		$this->addReference('user_user', $user);
		$manager->persist($user);

		// User
		$user = $this->createUser('admin', 'admin', 'admin@test.com');

		$user->addRole('ROLE_ADMIN');
		$this->addReference('user_admin', $user);
		$manager->persist($user);

		$manager->flush();
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @param string $email
	 *
	 * @return $this|static
	 */
	private function createUser(string $username, string $password, string $email)
	{
		$user = (new User())
			->setUsername($username)
			->setPlainPassword($password)
			->setEmail($email)
			->setEnabled(true);

		return $user;
	}
}