<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends FOSRestController
{
    /**
     * @Route("/", name="app_home")
     */
    public function indexAction(Request $request)
    {
        $view = $this->view()
                     ->setTemplate('@App/Default/index.html.twig');

        return $this->handleView($view);
    }
}
