#!/usr/bin/env bash

cd /var/www/provencale.interact.lu/www/html

echo '--- DATABASE LOADING ---'
php app/console doctrine:schema:drop --force --env=dev
php app/console doctrine:schema:update --force --env=dev
php app/console doctrine:fixtures:load --no-interaction --env=dev

echo '--- FILES LOADING ---'
composer install --no-suggest
php app/console assetic:dump --env=dev
php app/console cache:warmup --env=dev